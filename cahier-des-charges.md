Bonjour et bienvenue dans l'équipe Gilded Rose. Comme vous le savez, nous sommes une petite auberge avec un
emplacement privilégié dans une ville importante dirigée par un aubergiste sympathique nommé
Allison. Nous achetons et vendons également uniquement les meilleurs produits. Malheureusement, nos marchandises se dégradent constamment et perdent en qualité à mesure qu'elles approchent de leur date de vente. Nous avons un système en place qui met à jour notre inventaire pour nous. Il a été développé par un type pragmatique nommé Leeroy, qui est parti vers de nouvelles aventures. Votre tâche est d'ajouter la nouvelle fonctionnalité au système existant pour que nous puissions commencer à vendre une nouvelle catégorie d'articles. Voici tout d'abord une introduction à notre système:

- Tous les articles ont une valeur `sell_in` qui indique le nombre de jours dont nous disposons
pour vendre l'article
- Tous les articles ont une valeur `quality` qui indique la qualité de l'article
- À la fin de chaque journée, notre système réduit les deux valeurs pour chaque article

Assez simple, non ? Eh bien, c'est là que ça devient intéressant:

- Une fois la date limite de vente passée, la qualité se dégrade deux fois plus vite
- La qualité d'un article n'est jamais négative
- Le "AgedBrie" voit sa qualité augmenter avec l'âge
- La qualité d'un article n'est jamais supérieure à 50
- "Sulfuras", étant un objet légendaire, n'a pas de date limite de vente et ne perd pas en qualité
- "Backstage pass", comme le "AgedBrie", voit sa qualité s'améliorer quand la date de vente approche :
	- à partir de 10 jours avant la date du concert, la qualité augmente de 2 au lieu de 1
	- à partir de 5 jours avant la date du concert, la qualité augmente de 3 au lieu de 1
	- par contre, la qualité tombe à 0 après le concert

Nous avons récemment ajouté à notre catalogue un nouveau type d'article : *Conjured*. Il faut mettre à jour notre système avec la règle suivante : 

- Les objets *Conjured* se dégradent en qualité deux fois plus vite que les objets normaux

N'hésitez pas à apporter des modifications à la méthode `update_quality` et à ajouter le nouveau code tant que tout fonctionne toujours correctement. Cependant, ne modifiez pas la classe `cItem` ou l'attribut `items` car ceux-ci appartiennent au gobelin du coin qui va vous éliminez en un seul coup car il n'aime pas partager son code. 

Dernier point, un article ne peut jamais voir sa qualité augmenter au-dessus de 50, cependant "Sulfuras" est un objet légendaire et en tant que tel, sa qualité est de 80 et elle ne change jamais.

> Traduit et adapté de l'anglais par mes soins

* Autre traduction : https://github.com/emilybache/GildedRose-Refactoring-Kata/blob/master/GildedRoseRequirements_fr.md
* https://github.com/emilybache/GildedRose-Refactoring-Kata/blob/master/GildedRoseRequirements.txt
