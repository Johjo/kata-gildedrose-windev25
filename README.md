# Kata de refactoring : Gilded Rose

## introduction

Ce projet est un exercice de refactoring existant dans d'autres langages : 

- Le projet initial : https://github.com/NotMyself/GildedRose
- Le projet porté dans plusieurs langages : https://github.com/emilybache/GildedRose-Refactoring-Kata

Je l'ai porté en WinDev 25 pour le proposer en exercice aux personnes que je coache. Je le mets à disposition de la communauté pour que vous puissiez vous améliorer.

## Comment utiliser ce projet

Le moyen le plus simple est de cloner ce projet via git ou de télécharger le zip. Vous devrez ensuite lire et compléter le [cahier des charges](cahier-des-charges.md).

## Conseil et astuce

Je vous recommande de mettre en place un premier test automatique qui vous assure que le code fonctionne en permanence. Une fois que ce harnais de sécurité est en place, vous pourrez commencer à améliorer le code. Vous pourrez aussi ajouter des tests qui valident chaque cas d'utilisation.

Enfin, une fois le code prêt à accepter la nouvelle fonctionnalité, vous pourrez l'implémenter en pratiquant TDD.

## License

MIT

## Crédits

Ce travail est de [@TerryHughes](https://twitter.com/TerryHughes), [@NotMyself](https://twitter.com/NotMyself)

Le dépôt intial peut être trouvé à [https://github.com/NotMyself/GildedRose](https://github.com/NotMyself/GildedRose)